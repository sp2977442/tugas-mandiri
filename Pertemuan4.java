package Project;


import java.util.Scanner;
public class Pertemuan4 {
    public static void main(String[] args) {
        String grade ="";
        Scanner sc = new Scanner(System.in);
        System.out.println("Silahkan Masukan Nilai : ");
        int nilai  = sc.nextInt();
        if(nilai>0 && nilai<=50){
           grade = "E";
        }else if (nilai>50 && nilai<=60){
            grade = "D";
        }else if (nilai>60 && nilai<=70){
            grade = "C";
        }else if (nilai>70 && nilai<=80){
            grade ="B";
        }else if (nilai>80 && nilai<=100){
            grade ="A";
        }
        System.out.println("=================");
        System.out.println("Nilai Anda adalah :   "+nilai);
        System.out.println("Grade anda adalah :   "+grade);
        System.out.println("=================");
    }
    
}
